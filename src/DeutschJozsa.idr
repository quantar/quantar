{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module DeutschJozsa

import QC

deutschjozsa : {n : Nat} -> (BitString n -> BitString 1) -> QCircuit (n+1)
--deutschjozsa {n} f = (hadamard n || hadamard 1) . oracle f . (hadamard n || hadamard 1) . (identity n || not)
deutschjozsa {n} f =  (identity n || not) >> (hadamard n || hadamard 1) >> oracle f >> (hadamard n || hadamard 1)
