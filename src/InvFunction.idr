{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module InvFunction

import public Data.Vect
import public BitString

%access public export

data InvFunction : (n : Nat) -> Type where
     IF : (BitString n -> BitString n) -> InvFunction (n)

makeInv : (BitString n -> BitString m) -> InvFunction (n+m)
makeInv {n} {m} f  = IF (\x => (take n x) ++ ((drop n x) `xor` (f $ take n x)))

not : InvFunction 1
not = IF (\[x] => [not x])

cnot : InvFunction 2
cnot = IF (\[x,y] => [x, if x then (not y) else y])

total apply : InvFunction n -> (BitString n -> BitString n)
apply (IF f) = f

identity : InvFunction n
identity = IF id
