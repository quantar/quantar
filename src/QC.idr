{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module QC

import public InvFunction
import public QCTypes

%access public export

data QCircuit : (n : Nat) -> Type where
  Function : InvFunction n -> QCircuit n
  (||) : {k:Nat} -> {l:Nat} -> QCircuit k -> QCircuit l -> QCircuit (k+l)
  (.) : QCircuit n -> QCircuit n -> QCircuit n
  Hadamard : QCircuit 1
  Z : QCircuit 1

QState : (n : Nat) -> Type
QState n = BitString n -> Amplitude

(>>) : QCircuit n -> QCircuit n -> QCircuit n
c1 >> c2 = c2 . c1

not : QCircuit 1
not = Function not

cnot : QCircuit 2
cnot = Function cnot

identity : (n : Nat) -> QCircuit n
identity n = Function identity

oracle : (BitString n -> BitString m) -> QCircuit (n+m)
oracle = Function . makeInv

leftstate : BitString m -> QState (n+m) -> QState n
leftstate y s = \x => s (x++y)

rightstate : BitString n -> QState (n+m) -> QState m
rightstate y s = \x => s (y++x)

hadamard : (n : Nat) -> QCircuit n
hadamard Z     = Function identity
hadamard (S Z) = Hadamard
hadamard (S n) = Hadamard || hadamard n


singlequbit : Vect 4 Amplitude -> (QState 1 -> QState 1)
singlequbit [a,b,c,d] = \s => (\[x] => if x then c*(s [False])+d*(s [True])
                                            else a*(s [False])+b*(s [True]))


rx : Double -> (QState 1 -> QState 1)
rx theta = let htheta = theta/2 in singlequbit [      cos htheta :+ 0, -i*(sin htheta :+ 0),
                                                -i * (sin htheta :+ 0),    cos htheta :+ 0]

ry : Double -> (QState 1 -> QState 1)
ry theta = let htheta = theta/2 in singlequbit [      cos htheta :+ 0, -(sin htheta :+ 0),
                                                      sin htheta :+ 0,   cos htheta :+ 0]

rz : Double -> (QState 1 -> QState 1)
rz theta = let htheta = theta/2 in singlequbit [      cis (-htheta),     0,
                                                      0,                 cis htheta]
globalphase : Double -> (QState 1 -> QState 1)
globalphase alpha = \s => (\x => cis alpha * s x)

px : QState 1 -> QState 1
px = \s => (\x => s (map not x))

pi : QState 1 -> QState 1
pi = id

py : QState 1 -> QState 1
py = \s => (\[x] => if x then -i*s [False] else i*s [True])

pz : QState 1 -> QState 1
pz = \s => (\[x] => if x then -s [True] else s [False])

rn : Vect 4 Double -> Double 


-- I = zydecompose [0,0,0,0]
-- X = 
zydecompose : Vect 4 Double -> (QState 1 -> QState 1)
zydecompose [alpha,beta,gamma,delta] = globalphase alpha . rz beta . ry gamma . rz delta

identity' : QState 1 -> QState 1
identity' = zydecompose [0,0,0,0]

y' : QState 1 -> QState 1
y' = zydecompose [pi/2,0,pi,0]

z' : QState 1 -> QState 1
z' = zydecompose [pi/2,pi,0,0]

hadamard' : QState 1 -> QState 1
hadamard' = zydecompose [-pi/2,-pi,-pi/2,0]

apleft : {n : Nat} -> {m : Nat} -> (QState (n) -> QState (n)) -> QState (n+m) -> QState (n+m)
apleft {n} {m} c s = \(xy) => let (x,y) = splitAt n xy in (c $ leftstate y s) x

apright : {n : Nat} -> {m : Nat} -> (QState (m) -> QState (m)) -> QState (n+m) -> QState (n+m)
apright {n} {m} c s = \(xy) => let (x,y) = splitAt n xy in (c $ rightstate x s) y

zeros : {n : Nat} -> BitString n
zeros {n} = replicate n False

maxvals : {n : Nat} -> BitString n
maxvals {n} = replicate n True

zero : QState n
zero = \xs => if (xs == zeros) then 1 else 0

maxval : QState n
maxval = \xs => if (xs == maxvals) then 1 else 0

zgate : QState 1 -> QState 1
zgate = \s => (\x => if x==[False] then s x else -(s x))

eval : QCircuit n -> (QState n -> QState n)
eval ((||) {k} {l} left right) = ((apleft {n=k} {m=l} $ eval left) . (apright {n=k} {m=l} $ eval right))
eval Hadamard = \s => \x => 1/(sqrt 2 :+ 0) * (s([False]) + if (x==[True]) then -s([True]) else s([True]) )
eval (c1 . c2) = eval c1 . eval c2
eval (Function f) = \s => s . (apply f)
eval Z = zgate
--eval QIdentity = \s => s
--eval QNot = \s => (\x => s $ map not x)

toList : QState n -> List (BitString n, Amplitude)
toList {n} s = [(xs, s xs) | xs <- allVectors n]

tabularize : (QState n -> QState n) -> (QState n -> List (BitString n, Amplitude))
tabularize {n} c = \s => [(xs, c s xs) | xs <- allVectors n]

-- Apply a quantum circuit to a zero initialized quantum register
simulate : QCircuit n -> List (BitString n, Amplitude)
simulate c = (tabularize $ eval c) zero
