{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module Teleport

import QCMixedState

bobCorrection : (Bool, Bool) -> QCircuit 1
bobCorrection (False, False) = identity 1
bobCorrection (False, True)  = not
bobCorrection (True,  False) = Z
bobCorrection (True,  True)  = (Z .  not)

bellState : QCircuit 2
bellState = cnot . (hadamard 1 || identity 1) 

-- Teleports a qubit. This qubit is defined by the quantum circuit 'alice' applied to a qubit initialized to zero.
teleport : QCircuit 1 -> Mixed 3 ()
teleport alice = do gate (alice || bellState)    -- prepare the QBit of Alice and the entangled pair
                    gate (cnot || identity 1) -- interaction between entangled pair and to be transferred QBit
                    gate (hadamard 1 || identity 2)
                    bit0 <- measureBit 0
                    bit1 <- measureBit 1
                    gate (identity 2 || bobCorrection (bit0, bit1))
                    pure ()

