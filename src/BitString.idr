{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module BitString

import Data.Vect

%access public export

BitString : Nat -> Type
BitString n = Vect n Bool

allVectors : (n : Nat) -> List (BitString n)
allVectors Z     = [[]]
allVectors (S n) = [ x::xs | xs <- allVectors n, x <- [False, True] ]

xor : BitString n -> BitString n -> BitString n
xor x y = zipWith xor x y
 where
   xor : Bool -> Bool -> Bool
   xor (False) x = x
   xor (True)  x = not x
