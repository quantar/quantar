{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module Deutsch

import QCMixedState

deutsch : (BitString 1 -> BitString 1) -> QCircuit 2
deutsch f = (Hadamard || (Hadamard)) . oracle f . (Hadamard || Hadamard) . (identity 1 || not)

deutsch' : (BitString 1 -> BitString 1) -> QCircuit 2
deutsch' f = (identity 1 || not) >> (Hadamard || (Hadamard)) >> oracle f >> (Hadamard || Hadamard)

-- There are 4 possible functions 'f':

-- Constant 'False'
f1 : BitString 1 -> BitString 1
f1 = const [False]

-- Constant 'True'
f2 : BitString 1 -> BitString 1
f2 = const [True]

-- Identity
f3 : BitString 1 -> BitString 1
f3 = map id

-- Not
f4 : BitString 1 -> BitString 1
f4 = map not

{-
Example: Deutsch algorithm applied to all possible inputs:
simulate (gate $ deutsch f1)
simulate (gate $ deutsch f2)
simulate (gate $ deutsch f3)
simulate (gate $ deutsch f4)
-}
