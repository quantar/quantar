{-
    This file is part of QUANTAR.

    QUaNTAR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QUANTAR.  If not, see <https://www.gnu.org/licenses/>.
-}

module QCMixedState

import public QC

%access public export

data Mixed : (n : Nat) -> (res : Type) -> Type where
  MixS : (QState n -> List (Probability, QState n, res)) -> Mixed n res

implementation Functor (Mixed s) where
  map f (MixS s) = MixS (\q => [ (p, q', f res)  | (p, q', res) <- s q])

implementation Applicative (Mixed s) where
  pure x = MixS (\i => [(1.0, i, x)])
  (MixS f) <*> (MixS s) = MixS (\q => [ (p*p', q'', res res')  | (p, q', res) <- f q, (p', q'', res') <- s q' ])

implementation Monad (Mixed s) where
  (MixS f) >>= s        = MixS (\q => [ (p'*p, q'', res')  |  (p,q',res) <- f q, let (MixS s') = s res, (p', q'', res') <- s' q'  ])

applyQ : Mixed n res -> (QState n -> List (Probability, QState n, res))
applyQ (MixS x) = x

gate : QCircuit n -> Mixed n ()
gate c = MixS (\i => [(1, (eval c) i, ())])

total measureAll : Mixed n (BitString n)
measureAll {n} = MixS (\s => [ let c = s xs in (ampprob c,\ys => if (xs==ys) then 1 else 0,xs) | xs <- allVectors n ])

bitProb : Bool -> Fin n -> QState n -> Probability
bitProb {n} b m s = sum [ let c = s xs in ampprob c | xs <- allVectors n, index m xs == b ]

bitPrj : Bool -> Fin n -> QState n -> QState n
bitPrj {n} b m s =  let p = sqrt (bitProb b m s) in (\x => if (Data.Vect.index m x ==b) then (s x)/(p :+ 0) else 0)

measureBit : Fin n -> Mixed n Bool
measureBit m = MixS( \s => [ (bitProb False m s, bitPrj False m s, False), (bitProb True m s, bitPrj True m s, True)])

printable : List (Probability, QState n, a) -> List (Probability, List (BitString n, Amplitude), a)
printable xs = [ (x, toList y, z) | (x,y,z) <- xs ]

simulate : Mixed n a -> List (Probability, List (BitString n, Amplitude), a)
simulate m = printable $ applyQ m zero
